(function($){

	// Calc control object
	Calc = {

		_construct: function() {

			// Elements definition
			this.elm = {
				$calculator: $('#calculator'),
				$accommodationTotal: $('#ubytovani-celkem'),
				$accommodationPerson: $('#ubytovani-osoba'),
				$foodTotal: $('#strava-celkem'),
				$personTotal: $('#cena-osoba'),
				$total: $('#cena-celkem'),
			}

			this.cabins = ['tereza', 'hanka', 'sandra', 'linda-1', 'stan'];
			this.food = ['plnapenze', 'polopenze', 'snidane', 'obed', 'vecere'];

		},

		// Initialize on start
		init: function() {

			this._construct();

			this.run();

			$(window).scroll(function() {
				Calc.onScrollFn();
			});

			$(window).resize(function() {
				Calc.onResizeFn();
			});

			if(!(/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera)){
				//
			}

		},

		// Run on window resize
		onResizeFn: function() {
		},

		// Run on window scroll
		onScrollFn: function() {
		},

		// Run on start
		run: function() {
			var _this = this;

			this.elm.$calculator
				.find(':input')
				.change(function(event) {
					_this.calculate();
				})
			;
		},

		calculate: function() {
			var _this = this,
					totalPrice = 0,
					nights = $('#pocet-noci').val() || 1,
					totalPersons = 0;

			$.each(this.cabins, function(index, val) {
				if(nights > 0) {
					var persons = $('#pocet-osob-'+val).val() || 0;
					// cena chatky
					var cabinPrice = ( $('#pocet-'+val).val() || 0 ) * $('#cena-'+val).val() * nights;
					// cena za prvni noc
					var guestsPrice = persons * $('#cena-'+val+'-luzko-prvni').val();
					// cena za dalsi noc
					if(nights > 1) {
						guestsPrice += persons * (nights-1) * $('#cena-'+val+'-luzko-dalsi').val();
					}
					totalPersons += parseInt(persons);
					totalPrice += cabinPrice;
					totalPrice += guestsPrice;
				}
			});

			totalPrice = totalPrice || 0;
			totalPersons = totalPersons || 0;

			this.elm.$accommodationTotal.val(totalPrice);
			this.elm.$accommodationPerson.val( Math.ceil(totalPrice/totalPersons/nights) );

			var totalFood = 0;
			$.each(this.food, function(index, val) {
				var days = $('#'+val+'-dni').val() || 0;
				// cena stravy
				var foodPrice = ( ( $('#pocet-'+val+'-dite').val() || 0 ) * $('#cena-'+val+'-dite').val() + ( $('#pocet-'+val+'-dospely').val() || 0 ) * $('#cena-'+val+'-dospely').val() ) * days;
				totalFood += foodPrice;
			});

			this.elm.$foodTotal.val(totalFood);

			totalPrice += totalFood;

			this.elm.$personTotal.val( Math.ceil(totalPrice/totalPersons) );
			this.elm.$total.val(totalPrice);
		}

	}

	$(document).ready(function (){

		// Init web
		Calc.init();

	});

})(window.jQuery);
