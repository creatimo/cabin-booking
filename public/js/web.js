(function($){

	// Calc control object
	Calc = {

		_construct: function() {

			// Elements definition
			this.elm = {
				$calculator: 	$('#calculator'),
				$total: 			$('#calculator .price-total'),
				$cabinsSelect:$('#cabins'),
				$cabins: 			$('#calculator .cabins'),
				$cabinAdd: 		$('#calculator .cabin-add'),
				$cabinModel: 	$('#calculator .cabins .cabin-model'),
				$cabinExt: 		$('#calculator .cabins-control .ext')
			}

			// Cabins definition
			this.cabins = {
				tereza: {
					name: 'Tereza',
					countBeds: 7,
					priceCabin: 500,
					priceBadFirst: 200,
					priceBadNext: 100
				},
				hanka: {
					name: 'Hanka',
					countBeds: 6,
					priceCabin: 300,
					priceBadFirst: 200,
					priceBadNext: 100
				},
				sandra: {
					name: 'Sandra',
					countBeds: 4,
					priceCabin: 200,
					priceBadFirst: 200,
					priceBadNext: 100
				},
				linda3: {
					name: 'Linda 3',
					countBeds: 3,
					priceCabin: 100,
					priceBadFirst: 200,
					priceBadNext: 100
				},
				linda2: {
					name: 'Linda 2',
					countBeds: 2,
					priceCabin: 100,
					priceBadFirst: 200,
					priceBadNext: 100
				},
			}

			// Age definition
			this.age = {
				adult: {
					name: 'Dospělý'
				},
				child: {
					name: 'Dítě do 11 let'
				},
			}

			// Food definition
			this.food = {
				no: {
					name: 'Bez stravy',
					adult: 0,
					child: 0
				},
				half: {
					name: 'Polopenze',
					adult: 130,
					child: 90
				},
				fuel: {
					name: 'Plná penze',
					adult: 220,
					child: 150
				},
			}

		},

		// Initialize on start
		init: function() {

			this._construct();

			this.run();

			$(window).scroll(function() {
				Calc.onScrollFn();
			});
			
			$(window).resize(function() {
				Calc.onResizeFn();
			});

			if(!(/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera)){
				//
			}

		},

		// Run on window resize
		onResizeFn: function() {
		},

		// Run on window scroll
		onScrollFn: function() {
		},

		// Run on start
		run: function() {
			var _this = this;

			// Create new cabin
			this.elm.$cabinAdd.click(function(event) {
				event.preventDefault();
				var selectedCabin = _this.elm.$cabinsSelect.val();
				_this.addCabin(selectedCabin);
			});

			// Fill cabins selector
			$.each(this.cabins, function(index, val) {
				_this.elm.$cabinsSelect.append(
						$('<option>')
							.attr({
								value: index
							})
							.text(val.name)
					);
			});

			// Fill food selector
			$.each(this.food, function(index, val) {
				_this.elm.$cabinModel.find('.food').append(
						$('<option>')
							.attr({
								value: index
							})
							.text(val.name)
					);
			});

			// Fill age selector
			$.each(this.age, function(index, val) {
				_this.elm.$cabinModel.find('.age').append(
						$('<option>')
							.attr({
								value: index
							})
							.text(val.name)
					);
			});

			this.elm.$cabinsSelect
				.change(function() {
					var cabinCode = $(this).val();
					_this.showExt(cabinCode);
				});
			;
			this.showExt();

		},

		// Create new cabin in order
		addCabin: function(cabinCode) {
			var _this = this;

			var cabinConfig = this.cabins[cabinCode],
					$cabin = this.elm.$cabinModel.clone(),
					lastDays = this.elm.$cabins.find('.cabin:not(.cabin-model)').last().find('.days').val() | 1;

			// setup cabin holder
			$cabin
				.removeClass('cabin-model')
				.data('code', cabinCode)
				.find('.cabin-name')
				.text(cabinConfig.name)
			;

			$cabin
				.find('.days')
				.val(lastDays)
				.change(function() {
					_this.calculate();
				});
			;

			// bind events for cabin remove
			$cabin
				.find('.cabin-remove')
				.click(function(event) {
					event.preventDefault();
					_this.removeCabin($cabin);
				})
			;

			// bind events for guest add
			$cabin
				.find('.guest-add')
				.click(function(event) {
					event.preventDefault();
					_this.addGuest($cabin);
				})
			;

			// Add first guest
			_this.addGuest($cabin);

			this.elm.$cabins.append($cabin);
			$cabin.css('display','none').slideDown(600);
			this.calculate();
		},

		removeCabin: function(cabin) {
			var _this = this;
			cabin.slideUp(300, function(){
				$(this).remove();
				_this.calculate();
			});
		},

		addGuest: function(cabin) {
			var _this = this;

			var cabinCode = cabin.data('code'),
					cabinConfig = this.cabins[cabinCode],
					$guest = cabin.find('.guest-model').clone(),
					guests = cabin.find('.guests .guest'),
					guestsCount = guests.length,
					lastAge = guests.last().find('.age').val(),
					lastFood = guests.last().find('.food').val();

			if(guestsCount > cabinConfig.countBeds) {
				alert('Litujeme, ale chatka typu ' + cabinConfig.name + ' nabízí nejvýše ' + cabinConfig.countBeds + ' lůžek.');
				return;
			}

			$guest
				.removeClass('guest-model')
				.find('.guest-remove')
				.click(function(event) {
					event.preventDefault();
					_this.removeGuest($guest);
				});
			;

			$guest
				.find('.food')
				.val(lastFood)
				.change(function() {
					_this.calculate();
				});
			;

			$guest
				.find('.age')
				.val(lastAge)
				.change(function() {
					_this.calculate();
				});
			;
			
			cabin.find('.guests').append($guest);
			$guest.css('display','none').slideDown(300);
			this.calculate();
		},

		removeGuest: function(guest) {
			var _this = this;
			guest.slideUp(300, function(){
				$(this).remove();
				_this.calculate();
			});
		},

		calculate: function() {
			var _this = this,
					cabins = this.elm.$cabins.find('.cabin:not(.cabin-model)'),
					totalPrice = 0;
			
			$.map(cabins, function(cabin) {
				var cabinCode = $(cabin).data('code'),
						cabinConfig = _this.cabins[cabinCode],
						guests = $(cabin).find('.guest:not(.guest-model)'),
						days = $(cabin).find('.days').val(),
						guestsCount = guests.length,
						guestNo = 1;

				totalPrice += cabinConfig.priceCabin * days;
				if(guestsCount > 0) {
					totalPrice += cabinConfig.priceBadFirst * days;
					if(guestsCount > 1) {
						totalPrice += cabinConfig.priceBadNext * (guestsCount - 1) * days;
					}
				}
						
				$.map(guests, function(guest) {
					var food = $(guest).find('.food').val(),
							age = $(guest).find('.age').val(),
							priceFood = _this.food[food][age];

					$(guest).find('.guest-name').text('Host ' + guestNo);
					guestNo += 1;

					totalPrice += priceFood * days;
				});
			});

			this.elm.$total.text(totalPrice+',-');
		},

		showExt: function(cabinCode) {
			var _this = this,
					cabinConfig = cabinCode ? this.cabins[cabinCode] : this.cabins[Object.keys(this.cabins)[0]],
					ext;

			ext = 'Chata typu <b>' + cabinConfig.name + '</b> nabízí ubytování až pro <b>' + cabinConfig.countBeds + '</b> osob.';

			this.elm.$cabinExt.html(ext);
		}

	}


	$(document).ready(function (){

		// Init web
		Calc.init();

	});	

})(window.jQuery);
